var config = {
	title:"Casa Kowalkowski's Coronavirus Quarantine Quiz Soundboard",
	keys:"1QAZ2WSX3EDC4RFV5TGB6YHN7U",
	sources:[
		{song:'Queen - Radio Ga Ga.mp3',sounds:[{duration:18.72}]},
		{song:'Simple Minds - Alive And Kicking.mp3',sounds:[{duration:18.6}]},
		{song:'Lipps Inc - Funky Town.mp3',sounds:[{duration:7.85}]},
		{song:'Billy Idol - White Wedding.mp3',sounds:[{duration:1.62}]},

		{song:'Men Without Hats - Safety Dance.mp3',sounds:[{duration:2.33}]},
		{song:'Orchestral Manoeuvres In The Dark - Enola Gay.mp3',sounds:[{duration:3.34}]},
		{song:'U2 - With Or Without You.mp3',sounds:[{duration:7.635}]},
		{song:'Pigbag - Papas Got A Brand New Pigbag.mp3',sounds:[{duration:3.442}]},

		{song:'Steve Miller Band - Abracadabra.mp3',sounds:[{duration:7.56}]},
		{song:'Tears For Fears - Shout.mp3',sounds:[{duration:4.89 }]},
		{song:'INXS - Need You Tonight - 1.mp3',sounds:[{duration:2.2}]},
		{song:'INXS - Need You Tonight - 2.mp3',sounds:[{duration:4.37}]},

		{song:'The Who - Who Are You - Title.mp3',sounds:[{duration:6.15}]},
		{song:'The Who - Who Are You - Tick.mp3',sounds:[{duration:18.78 }]},
		{song:'Karel Fialka - Hey Matthew.mp3',sounds:[{duration:2.53}]},
		{song:'Paul Simon - You Can Call Me Al.mp3',sounds:[{duration:7.51}]},

		{song:'Huey Lewis and The News - The Power of Love.mp3',sounds:[{duration:8.04}]},
		{song:'The Bangles - Eternal Flame.mp3',sounds:[{duration:3.03}]},
		{song:'Black - Wonderful Life.mp3',sounds:[{duration:9.05}]},
		{song:'New Order - Blue Monday.mp3',sounds:[{duration:7.35}]},

		{song:'Abba - Money Money Money.mp3',sounds:[{duration:6}]},
		{song:'Abba - Money Money Money - 2.mp3',sounds:[{duration:0.985}]},
		{song:'Fleetwood Mac - Everywhere - Tick.mp3',sounds:[{duration:8.36}]},
		{song:'Fleetwood Mac - Everywhere.mp3',sounds:[{duration:4.18}]},

		{song:'The Belle Stars - Sign Of The Times.mp3',sounds:[{duration:4.28}]},
		{song:'David Bowie - Ashes To Ashes.mp3',sounds:[{duration:12.12}]}
	]
};