HTML = {
	addElement: function(tagName, attributes, content, parent) {
		parent = parent || document.body;
		var element = document.createElement(tagName);
		parent.appendChild(element);

		if(attributes) {
			for(var i in attributes) {
				if(attributes.hasOwnProperty(i)) {
					element[i] = attributes[i];
				}
			}
		}

		if(content !== undefined && content !== false) {
			element.textContent = content;
		}

		return element;
	},

	addScript: function(filename, callback) {
		HTML.addElement('script', {src:filename, onload:callback}, false, document.head);
	},

	addStyle: function(filename) {
		HTML.addElement('link', {rel:'stylesheet',href:filename}, false, document.head);
	},

	log: function(message) {
		if(localStorage.getItem('logging')) {
			var time = new Date().getTime();
			var millisecond = time % 1000;
			time = parseInt(time / 1000);
			var second = time % 60;
			time = parseInt(time / 60);
			minute = time % 60;
			console.log(minute + ':' + second + '.' + millisecond + ' ' + message);
		}
	}
};