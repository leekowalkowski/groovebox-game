new function() {
	var PREPARATION_TIME = config.beatLength;

	Event.listen('Game.nextStep', function(step) {
		Queue.add('Hint', function(){step.sound.button.classList.add('next');}, step.leadTime - PREPARATION_TIME);
		Queue.add(function(){step.sound.button.classList.remove('next');}, step.leadTime);
	});
};