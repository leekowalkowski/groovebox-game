new function() {
	var parent = HTML.addElement('ul', {className:'vocal'});

	Event.listen('Mixer.sound', function(sound) {
		if(sound.vocal) {
			var li = HTML.addElement('li', false, sound.vocal, parent);
			Queue.add(
				function(){li.classList.add('animate');}, 0,
				function(){li.parentNode.removeChild(li);}, 5
			);
		}
	});
}