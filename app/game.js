new function() {
	var PERFECT = config.beatLength / 16;
	var GOOD = PERFECT * 2;
	var ALMOST = GOOD * 2;
	var demoMode;
	var patternIndex;
	var playing;

	new function() {
		for(var i = 0; i < config.pattern.length; i++) {
			var soundNumber = config.pattern[i];
			var durationMultiplier = (soundNumber - parseInt(soundNumber)) || 1;
			var soundNumber = parseInt(soundNumber);
			var sound = config.sounds[soundNumber];
			var duration = sound.duration * durationMultiplier;

			config.pattern[i] = {
				sound:sound,
				duration:duration
			};
		}
	};

	HTML.addScript('app/hint.js');
	HTML.addScript('app/patternDisplay.js');
	HTML.addScript('app/scoreDisplay.js');
	HTML.addScript('app/tutorial.js');

	Event.listen('Groovebox.game', function(_demoMode) {
		silence();
		playing = true;
		demoMode = _demoMode;
		Queue.add('Game', playPattern, 0);
	});

	Event.listen('Mixer.sound', score);
	Event.listen('Mixer.silence', silence);

	function time() {
		return new Date().getTime() / 1000;
	}

	function silence() {
		if(playing) {
			playing = false;
			Queue.empty();

			for(var i = 0; i < config.pattern.length; i++) {
				config.pattern[i].scored = false;
				config.pattern[i].played = false;
			}
		}

		patternIndex = -1;
	}

	function playPattern() {
		var timeToNext;
		var thisStep = config.pattern[patternIndex];
		var nextStep = config.pattern[patternIndex + 1];

		if(thisStep) {
			timeToNext = thisStep.duration;
		} else {
			timeToNext = nextStep.sound.start - config.intro;
			Groovebox.intro();
		}

		if(nextStep) {
			nextStep.leadTime = timeToNext;
			nextStep.expected = time() + timeToNext;

			if(demoMode) {
				Queue.add('Demo', function(){Groovebox.sound(nextStep.sound);}, timeToNext);
			}

			Queue.add('Score', score, Math.min(timeToNext + ALMOST, timeToNext + nextStep.duration / 2));
			Queue.add('Game', playPattern, timeToNext);
			Event.fire('Game.nextStep', nextStep);
		} else {
			Queue.add('Game', function(){Groovebox.outro();}, timeToNext - ALMOST);
			Event.fire('Game.complete');
			if(demoMode) {
				Queue.add('Game', Groovebox.demo, 300);
			}
		}

		patternIndex++;
	}

	function reactionTime(time, step) {
		if(step) {
			step.reactionTime = Math.abs(step.expected - time);
		}
	}

	function score(sound) {
		if(playing)
		{
			var result;
			var nextStep = config.pattern[patternIndex];
			var lastStep = config.pattern[patternIndex - 1];
			var closestStep = nextStep ? nextStep : lastStep;
			var now = time();
			reactionTime(now, nextStep);
			reactionTime(now, lastStep);

			if(nextStep && lastStep && (nextStep.reactionTime > lastStep.reactionTime)) {
				closestStep = lastStep;
			}

			if(!closestStep.scored) {
				closestStep.scored = true;
				result = ScoreDisplay.MISS;

				if(closestStep.sound === sound) {
					if(PERFECT > closestStep.reactionTime) {
						result = ScoreDisplay.PERFECT;
					} else if(GOOD > closestStep.reactionTime) {
						result = ScoreDisplay.GOOD;
					} else if(ALMOST > closestStep.reactionTime) {
						result = ScoreDisplay.ALMOST;
					}
				}

				var playerWasEarly = closestStep === nextStep;

				Event.fire('Game.score', {result: result, early: playerWasEarly});

				if(result !== ScoreDisplay.MISS) {
					if(playerWasEarly) {
						Queue.processNow('Game');
					} else {
						Queue.requeue('Game');
					}
				}
			}
		}
	}
};