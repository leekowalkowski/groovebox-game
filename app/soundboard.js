Soundboard = new function() {
	let self = this;
	let folder = location.hash.substring(1) || 'quiz-night';
	let volume = 1;
	let loop = true;
	let xfade = true;
	let fadeSpeed = 2;
	let currentSound = {};

	self.setVolume = function(newVolume) {
		volume = newVolume;
		if(currentSound.source) {
			currentSound.source.audioPlayer.fade(volume, 0.5);
		}
	};

	self.sound = function(sound) {
		sound.playing = true;
		sound.fading = false;
		Queue.remove(sound.key);

		if(xfade) {
			config.sounds.forEach(otherSound => {
				if(otherSound !== sound) {
					if(otherSound.playing && !otherSound.fading) {
						otherSound.fading = true;
						otherSound.source.audioPlayer.fade(0, fadeSpeed);
						Queue.add('XFade', () => {
							otherSound.source.audioPlayer.stop();
							Queue.remove(otherSound.key);
							otherSound.playing = false;
						}, fadeSpeed);
					}
				}
			});
			sound.source.audioPlayer.fade(0);
			sound.source.audioPlayer.play(sound.start, sound.duration);
			sound.source.audioPlayer.fade(volume, fadeSpeed);
		} else {
			if(currentSound.source) {
				currentSound.source.audioPlayer.stop();
			}
			sound.source.audioPlayer.fade(volume);
			sound.source.audioPlayer.play(sound.start, sound.duration);
		}

		if(loop) {
			Queue.add(sound.key, () => self.loop(sound), sound.duration);
		}

		currentSound = sound;

		Event.fire('Mixer.sound', sound);
	};

	self.loop = function(sound) {
		sound.source.audioPlayer.play(sound.start, sound.duration);

		if(loop) {
			Queue.add(sound.key, () => self.loop(sound), sound.duration);
		}

		if(!sound.fading) {
			Event.fire('Mixer.sound', sound);
		}
	}

	self.silence = function() {
		Queue.empty();
		config.sounds.forEach(sound => sound.source.audioPlayer.stop);
		Event.fire('Mixer.silence')
	};

	self.toggleLoop = function() {
		loop = !loop;
		if(!loop) {
			config.sounds.forEach(sound => Queue.remove(sound.key));
		}
	}

	self.toggleXFade = function() {
		xfade = !xfade;
		console.log(`XFade ${xfade}`);
	}

	window.onclick = function(e) {
		if(e.target.sound) {
			self.sound(e.target.sound);
		}
	};

	HTML.addScript('app/event.js');
	HTML.addScript('app/queue.js');
	HTML.addScript('app/timeBar.js');

	HTML.addScript('app/audioPlayer.js', function(){
		HTML.addScript('app/keyboard.js', function(){
			HTML.addScript(folder + '/config.js', function () {
				document.title = config.title;
				document.querySelector('h1').textContent = config.title;
				config.keys = config.keys || '1234QWERASDFZXCV';

				var main = document.querySelector('main');
				config.sounds = [];

				config.sources.forEach(source => {
					source.audioPlayer = new AudioPlayer(folder + '/' + source.song);
					source.beatLength = 60 / (source.bpm || 60);
					source.offset = source.offset || 0;
					source.sounds.forEach(sound => {
						var key = config.keys.charAt(config.sounds.length);
						sound.source = source;
						sound.button = HTML.addElement('button', false, key, main);
						sound.button.sound = sound;
						sound.duration = sound.duration * source.beatLength;
						sound.start = (sound.start || 0) * source.beatLength + source.offset;
						sound.key = key;
						config.sounds.push(sound);
					});
				});

				if(config.keys.length !== config.sounds.length) {
					console.log('Number of keys does not match the number of sounds');
				}
					
				Keyboard.assign(
					{
						'Escape': self.silence,
						' ': self.silence,
						'l': self.toggleLoop,
						'/': self.toggleXFade,
						'=': function(){self.setVolume(1);},
						'-': function(){self.setVolume(.66);},
						'0': function(){self.setVolume(.25);},
						'9': function(){self.setVolume(.1);},
						'8': function(){self.setVolume(0);},
					}
				);

				Keyboard.default(
					function(e) {
						var key = e.key.toUpperCase();
						var index = config.keys.indexOf(key);

						if(index > -1) {
							self.sound(config.sounds[index]);
						}
					}
				);
			});
		});
	});
};