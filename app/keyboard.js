Keyboard = new function() {
	var callbacks = {};
	var defaults = [];
	var down = [];

	this.on = function(key, callback) {
		callbacks[key.toLowerCase()] = callback;
	};

	this.assign = function(map) {
		for(var key in map) {
			callbacks[key.toLowerCase()] = map[key];
		}
	};

	this.default = function(callback) {
		defaults.push(callback)
	};

	this.isPressed = function(key) {
		return down[key.toLowerCase()] || false;
	};

	window.addEventListener('keydown', function(e) {
		var key = e.key.toLowerCase();
		if(!down[key]) {
			if(callbacks[key]) {
				callbacks[key](e);
			} else {
				for(var i = 0; i < defaults.length; i++) {
					defaults[i](e);
				}
			}
		}

		down[key] = true;
	});

	window.addEventListener('keyup', function(e) {
		down[e.key.toLowerCase()] = false;
	});
}