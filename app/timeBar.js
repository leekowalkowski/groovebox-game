new function() {
	var playing = [];

	function setTransitionDuration(button, duration) {
		Queue.add(function(){
			button.style.transitionDuration = duration + 's';
			duration ? button.classList.add('playing') : button.classList.remove('playing');
		}, duration ? 0.04 : 0);
	}

	function start(sound) {
		stopAll();
		playing[sound.key] = sound;
		setTransitionDuration(sound.button, sound.duration - 0.1);
		Queue.replace('TimeBar', stopAll, sound.duration);
	}

	function stop(sound) {
		setTransitionDuration(sound.button, 0);
	}

	function stopAll() {
		for(var key in playing) {
			if(playing[key]) {
				stop(playing[key]);
			}
		}
	}

	Event.listen('Mixer.sound', start);
	Event.listen('Mixer.silence', stopAll);
	Event.listen('Groovebox.game', stopAll);
}