new function() {
	var parent = HTML.addElement('ul', {className: 'tutorial'});
	var instruction = HTML.addElement('li', false, false, parent);

	function queueInstruction(text, time, duration) {
		Queue.add('Tutorial' + time, function() {
			parent.classList.add('show');
			instruction.style.opacity = 0;
			Queue.add(function() {instruction.textContent = text; instruction.style.opacity = 1}, .25);
			Queue.add(function() {parent.style.height = instruction.scrollHeight + 'px'}, .26);
		}, time);
	}

	function remove() {
		parent.classList.remove('show');
	}

	Event.listen('Groovebox.game', function() {
		var introDuration = config.pattern[0].sound.start - config.intro;

		var instructions = config.instructions || [
			'The steps will appear from the right.',
			'They will move towards the taget on the left.',
			'When they reach the target, play the sound!',
			'Get ready!',
			'GO!'
		];

		instructions = instructions.concat([]); // create a copy;

		var duration = introDuration / instructions.length;
		var time = 0;


		while(instructions.length) {
			queueInstruction(instructions.shift(), time, duration);
			time += duration;
		}

		Queue.add('Tutorial', remove, time);
	});

	Event.listen('Mixer.silence', remove);
};