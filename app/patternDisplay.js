new function() {
	var SCROLL_SPEED = 100;
	var style;
	var scrollPosition;
	var parent;

	new function() {
		parent = HTML.addElement('div', {className: 'pattern off'});
		var instructions = HTML.addElement('ul', false, false, parent);
		var width = (config.sounds[0].start - config.intro) * SCROLL_SPEED;
		style = instructions.style;

		for(var i = 0; i < config.pattern.length; i++) {
			var sound = config.pattern[i].sound;
			var instruction = HTML.addElement('li', false, sound.key, instructions);
			instruction.style.left = width + 'px';
			width += config.pattern[i].duration * SCROLL_SPEED;
		}

		style.width = width + 30 * config.pattern.length + 'px';
	};

	function scroll(amount, time) {
		scrollPosition += amount;

		if(time !== undefined) { 
			style.transitionDuration = time + 's';
		}

		style.transform = 'translateX(' + scrollPosition + 'px)';
	}

	function reset() {
		parent.classList.add('off');
		scrollPosition = 0;
		scroll(0, 0);
	}

	Event.listen('Groovebox.game', reset);
	Event.listen('Groovebox.silence', reset);

	Event.listen('Game.nextStep', function(step) {
		parent.classList.remove('off');
		scroll(-step.leadTime * SCROLL_SPEED, step.leadTime);
	});

	Event.listen('Game.score', function(score) {
		if(score.result !== ScoreDisplay.MISS) {
			if(score.early) {
				scroll(0.0001, 0);
			} else {
				scroll(0.0001);
			}
		}
	});

	Event.listen('Game.complete', function () {
		scroll(-config.beatLength * SCROLL_SPEED, config.beatLength);
		parent.classList.add('off');
	});
};