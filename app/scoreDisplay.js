ScoreDisplay = new function() {
	var parent = HTML.addElement('ul', {className: 'result'});
	var totalParent = HTML.addElement('ul', {className: 'score off'});
	var total = 0;

	this.PERFECT = {
		points:250,
		text:'Perfect!',
		className:'perfect'
	};

	this.GOOD = {
		points:100,
		text:'Good',
		className:'good'
	};

	this.ALMOST = {
		points:50,
		text:'Almost',
		className:'almost'
	};

	this.MISS = {
		points:0,
		text:'MISS',
		className:'miss'
	};

	Event.listen('Game.score', function(score) {
		var miss = score.result === ScoreDisplay.MISS;
		var li = HTML.addElement('li', {className:score.result.className}, score.result.text, parent);

		if(!miss) {
			total += score.result.points;
			var totalLi = HTML.addElement('li', false, total, totalParent);
		}

		Queue.add(
			function() {
				li.classList.add('animate');

				if(!miss) {
					totalParent.firstChild.classList.remove('animate');
					totalParent.firstChild.classList.add('off');
					totalLi.classList.add('animate');
				}
			}, 0,

			function() {
				if(!miss) {
					totalParent.removeChild(totalParent.firstChild);
				}
			}, .25,

			function() {
				li.parentNode.removeChild(li);
			}, 3
		);
	});

	Event.listen('Groovebox.game', function() {
		total = 0;

		while(totalParent.childNodes.length) {
			totalParent.removeChild(totalParent.firstChild);
		}

		HTML.addElement('li', {className:'animate'}, 0, totalParent);
		totalParent.classList.remove('off', 'complete');
	});

	Event.listen('Mixer.silence', function() {
		totalParent.classList.add('off');
		totalParent.classList.remove('complete');
	});

	Event.listen('Game.complete', function() {
		totalParent.classList.add('complete');
	});
};