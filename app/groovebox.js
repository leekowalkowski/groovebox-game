Groovebox = new function() {
	var self = this;
	var folder = location.hash.substring(1) || 'ghostbusters';
	var audioPlayer;
	var game;
	var volume = 1;

	self.setVolume = function(newVolume) {
		volume = newVolume;
		audioPlayer.fade(volume, 0);
	};

	self.sound = function(sound) {
		audioPlayer.play(sound.start, sound.duration);
		Event.fire('Mixer.sound', sound);
	};

	self.silence = function() {
		Queue.empty();
		audioPlayer.stop();
		audioPlayer.fade(volume, 0);
		Event.fire('Mixer.silence')
	};

	self.intro = function() {
		audioPlayer.fade(0, 0);
		audioPlayer.fade(volume, 1);
		audioPlayer.play(config.intro);
		Event.fire('Mixer.intro');

	};

	self.outro = function() {
		self.sustain();
		audioPlayer.fade(0, 10);
		Queue.add(function(){
			audioPlayer.stop();
			audioPlayer.fade(volume, 0);
		}, 10);
		Event.fire('Mixer.outro');
	};

	self.sustain = function() {
		audioPlayer.play();
		Event.fire('Mixer.sustain');
	};

	self.demo = function() {
		Event.fire('Groovebox.game', true);
	};

	self.game = function() {
		Event.fire('Groovebox.game', false);
	};

	window.onclick = function(e) {
		if(e.target.sound) {
			self.sound(e.target.sound);
		}
	};

	HTML.addScript('app/event.js');
	HTML.addScript('app/queue.js');
	HTML.addScript('app/timeBar.js');
	HTML.addScript('app/vocal.js');

	HTML.addScript('app/keyboard.js', function() {
		Keyboard.assign(
			{
				'Escape': self.silence,
				'.': self.sustain,
				'$': self.outro,
				'^': self.intro,
				'#': self.demo,
				'=': self.game
			}
		);

		Keyboard.default(
			function(e) {
				var key = e.key.toUpperCase();
				var index = config.keys.indexOf(key);

				if(index > -1) {
					self.sound(config.sounds[index]);
				}
			}
		);
	});

	HTML.addScript(folder + '/config.js', function () {
		config.beatLength = 60 / config.bpm;
		config.offset = config.offset || 0;
		config.intro = config.intro || config.offset;
		config.keys = config.keys || '1234QWERASDFZXCV';

		if(config.keys.length !== config.sounds.length) {
			console.log('Number of keys does not match the number of sounds');
		}

		document.title = config.title;
		document.querySelector('h1').textContent = config.title;
		HTML.addStyle(folder + '/style.css');
		var main = document.querySelector('main');

		for(var i = 0; i < config.keys.length; i++) {
			var key = config.keys.charAt(i);
			var sound = config.sounds[i];
			sound.button = HTML.addElement('button', false, key, main);
			sound.button.sound = sound;
			sound.duration = sound.duration * config.beatLength;
			sound.start = sound.start * config.beatLength + config.offset;
			sound.key = key;
		}

		HTML.addScript('app/audioPlayer.js', function() {
			audioPlayer = new AudioPlayer(folder + '/' + config.song);
		});

		HTML.addScript('app/game.js');

		for(var i = 0; i < config.plugins.length; i++) {
			HTML.addScript(folder + '/' + config.plugins[i] + '.js');
		}
	});
};