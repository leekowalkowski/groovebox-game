Event = new function() {
	var events = {};

	this.listen = function(name, callback) {
		if(!events[name]) {
			events[name] = [callback];
		} else {
			events[name].push(callback);
		}
	};

	this.fire = function(name, eventData) {
		if(events[name] && events[name].length) {
			for(var i = 0; i < events[name].length; i++) {
				events[name][i](eventData);
			}
		}
	};
};