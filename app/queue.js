Queue = new function() {
	var timers = [];
	var MIN_TIMEOUT = 20;

	function isArray(variable) {
		return variable !== undefined && variable.constructor === [].constructor;
	}

	function queue(callback, timeout) {
		return setTimeout(callback, Math.max(parseInt(timeout * 1000), MIN_TIMEOUT));
	}

	this.add = function() {
		var start = 0;
		var id = false;
		var timer = [];

		if(arguments.length % 2 === 1) {
			start++;
			id = arguments[0];
		}

		for(var i = start; i < arguments.length; i += 2) {
			timer.push(
				{
					callback: arguments[i],
					timeout: arguments[i + 1],
					id: queue(arguments[i], arguments[i + 1])
				}
			);

			if(id) {
				timers[id] = timer;
			}
		}
	};

	this.replace = function() {
		if(arguments.length % 2 === 1) {
			Queue.remove(arguments[0]);
		}

		Queue.add.apply(this, arguments);
	}

	this.remove = function(id) {
		if(isArray(timers[id])) {
			for(var i = timers[id].length; i > 0; i--) {
				var timer = timers[id].shift();
				clearTimeout(timer.id);
			}
		}
	};

	this.requeue = function(id) {
		if(isArray(timers[id])) {
			for(var i = 0; i < timers[id].length; i++) {
				var timer = timers[id][i];
				clearTimeout(timer.id);
				timer.id = queue(timer.callback, timer.timeout);
			}
		}
	};

	this.processNow = function(id) {
		if(isArray(timers[id])) {
			for(var i = timers[id].length; i > 0; i--) {
				var timer = timers[id].shift();
				clearTimeout(timer.id);
				timer.callback();
			}
		}
	};

	this.empty = function() {
		for(var id in timers) {
			if(timers.hasOwnProperty(id)) {
				Queue.remove(id);
			}
		}
	};
};