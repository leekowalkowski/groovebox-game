function AudioPlayer(soundFile, volume) {
	var self = this;
	var audio = document.createElement('audio');
	var fadeInterval;

	audio.src = soundFile;
	audio.volume = volume || 1;

	self.play = function(time, duration) {
		if(time !== undefined) {
			audio.currentTime = time;
		}

		audio.play();

		if(duration) {
			Queue.replace('AudioPlayer' + soundFile, self.stop, duration);
		} else {
			Queue.remove('AudioPlayer' + soundFile);
		}
	};

	self.stop = function() {
		audio.pause();
	};

	self.fade = function(volume, duration) {
		if(fadeInterval) {
			clearInterval(fadeInterval);
		}

		if(duration > 0) {
			var difference = (volume - audio.volume) / 50 / duration;
			fadeInterval = setInterval(function(){
				var newVolume = audio.volume + difference;
				audio.volume = Math.max(0, Math.min(1, newVolume));

				if(parseInt(audio.volume * 100) === parseInt(volume * 100)) {
					audio.volume = volume;
					clearInterval(fadeInterval);
				}
			}, 20);
		} else {
			audio.volume = volume;
		}
	};
};