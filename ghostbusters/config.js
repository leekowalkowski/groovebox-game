var config = {
	title:'Ghostbusters Groovebox',
	song:'Ray Parker Jr - Ghostbusters Theme.mp3',
	bpm:116,
	offset:.17,
	intro:.8,
	introx:13.1,
	sounds:[
		{start:29,     duration:2},
		{start:31,     duration:2},
		{start:39,     duration:2},
		{start:160.5,  duration:1,     vocal:'OW!'},
		{start:41,     duration:4},
		{start:45,     duration:4},
		{start:65,     duration:3,     vocal:'Who you gonna call?'},
		{start:68,     duration:3.125, vocal:'Ghostbusters!'},
		{start:89.25,  duration:2.9},
		{start:92.25,  duration:5},
		{start:97.25,  duration:2.9},
		{start:100.25, duration:5,     vocal:"I ain't 'fraid of no ghost!"},
		{start:314,    duration:4},
		{start:318,    duration:4},
		{start:310,    duration:4},
		{start:262,    duration:4,     vocal:"Yeh-yeah yeh-yeah!"}
	],
	pattern:[
		0,1,0,1,0.98,2,
		0,1,0,1,0,1.42,7,2,
		0,1,0,1.9,6.725,1,
		0,1.5,3.9,6,7,2,
		4,5,4.72,7,1,
		4,5,4.72,7,2.5,3,
		8,9,10,11,
		8,9,10,11.57,15,
		0,1,0,1.42,7,1,
		4,5,4.72,7,2.5,3,
		12,13,12,14,
		12,13,12,14.55,15.69,3.8,
		6.725,1.9,6.725,1.9,6.725,1.9,6,7,2.5,3,
		8,9,10,11,
		8,9,10,11.57,7,3,
		4.715,7,1,4.715,7,1,4.715,7,1,4.715,7,2,
		12,13
	],
	patternx:[0, 13],
	plugins:[
		'jumpScare'
	],
	instructions:[
		"The steps will appear from the right",
		"Don't play them until they reach the target",
		"It's easiest to use the keyboard",
		"Don't be afraid...",
		"Are you ready?",
		"GO!"
	]
};