new function() {
	Groovebox.setVolume(.5);
	var scream = new AudioPlayer('ghostbusters/Scary - Scream.mp3');
	var active = false;
	var probability;

	Event.listen('Groovebox.game', function(demo) {
		active = !demo;
		probability = 0;
	});

	function deactivate() {
		active = false;
	}

	Event.listen('Groovebox.silence', deactivate);

	Event.listen('Game.score', function(score) {
		if(active) {
			probability += Math.random() / 20;

			if(probability >= 1) {
				scare();
				probability = 0;
			}
		}
	});

	function scare() {
		duration = Math.random() * 1.75 + .25;
		scream.play(Math.random() * 2, duration);
		Queue.add(
			function(){document.body.classList.add('scare')}, 0,
			function(){document.body.classList.remove('scare')}, duration
		);
	}

	Keyboard.assign({
		'*': scare
	});
}